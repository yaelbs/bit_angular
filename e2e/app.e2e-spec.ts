import { BitAngularPage } from './app.po';

describe('bit-angular App', function() {
  let page: BitAngularPage;

  beforeEach(() => {
    page = new BitAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
