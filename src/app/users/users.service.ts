import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class UsersService {

  //removed when added data server
  /*users = [
    {name:'John',email:'john@gmail.com'},
    {name:'Jack',email:'jack@gmail.com'},
    {name:'Alice',email:'alice@yahoo.com'}
  ]*/

  private _url = "http://jsonplaceholder.typicode.com/users";

  //changed when added data server
  /*getUsers(){
		return this.users;
  constructor() { }}*/

  constructor(private _http: Http) { }

  getUsers(){
		return this._http.get(this._url)
			.map(res => res.json()).delay(2000)
  }
}
